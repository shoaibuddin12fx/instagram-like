import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SqliteService } from 'src/app/services/sqlite.service';

@Component({
  selector: 'app-fbpost',
  templateUrl: './fbpost.component.html',
  styleUrls: ['./fbpost.component.scss'],
})
export class FbpostComponent implements OnInit {

  _item: any;
  user: any;
  get item(): any {
    return this._item;
  }
  @Input() set item(value: any){
    this._item = value;
    console.log(value);
    this.getUser(value.userId);

  };

  async getUser(user_id){
    console.log(user_id);
    this.user = await this.sqlite.getUserById(user_id);
    console.log(this.user);
  }

  @Output('userposts') userposts: EventEmitter<any> = new EventEmitter<any>() 

  constructor(public sqlite: SqliteService) { 

  }

  ngOnInit() {}

}
